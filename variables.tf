variable "student_name" {
  description = "Name of the student for whom the infrastructure will be created"
  type        = string
}

variable "location" {
  description = "Azure region where the resources will be created"
  type        = string
}

variable "environment" {
  description = "Environment for which the resources will be created"
  type        = string
}
